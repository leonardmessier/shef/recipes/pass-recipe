include .env
export

CONTAINER_TEST := $(shell command -v container-structure-test 2>/dev/null)

build:
		@shef build install.recipe build/install
install:
		@/bin/bash build/install

build-docker:
		@docker-compose build pass-recipe

test: test-acceptance
test-acceptance:
ifndef CONTAINER_TEST
	@echo "container-structure-test is not available. Follow instructions here : https://github.com/GoogleContainerTools/container-structure-test"
	exit 1
endif

	@container-structure-test test --image registry.gitlab.com/leonardmessier/shef/recipes/pass-recipe:$(RECIPE_VERSION) --config tests/acceptance/cst.yml
	@echo "\033[32m✓\033[0m Acceptance tests passed"

.PHONY: install build build-docker
